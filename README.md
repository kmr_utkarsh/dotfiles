# dotfiles

Configuration files for applications I use at work and personal projects.

![desktop](assets/desktop.png)


## Installation

At this point, the installation is done manually, but there are plans to automate this.

- The (Neo)Vim configuration is fully-automated, will install vim-plug and the plugins at first start.
- Bash configuration isn't automated yet, especially for tools like [SDKMAN](https://sdkman.io/) and [nvm](https://github.com/nvm-sh/nvm)

## Roadmap

- [] Explore tools like Stow, or just roll your own installer.
- [] Add more stuff to it (tmux, git, custom scripts)

## Thanks

- Zach Holman's Dotfiles: https://github.com/holman/dotfiles


