return {
  {
    'rebelot/kanagawa.nvim',
    priority = 1000,
    opts = {
      compile = false,
      transparent = true,
      theme = 'dragon',
      background = { dark = 'dragon', light = 'lotus' },
      colors = {
        theme = { all = { ui = { bg_gutter = 'none' } } },
      },
    },
    build = ':KanagawaCompile',
    init = function()
      vim.cmd.colorscheme 'kanagawa'
      -- vim.cmd.hi 'Comment gui=none'
    end,
  },
}
-- vim: ts=2 sts=2 sw=2 et
