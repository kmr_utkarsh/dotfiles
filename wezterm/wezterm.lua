local wezterm = require("wezterm")
local config = wezterm.config_builder()

config.color_scheme = "Kanagawa (Gogh)"

config.font = wezterm.font({ family = "GeistMono Nerd Font", weight = "Regular" })
config.font_size = 13.0
-- config.line_height = 1.1

config.default_cursor_style = "BlinkingBlock"

config.use_fancy_tab_bar = true
config.tab_max_width = 28
config.colors = {
	foreground = "#DCD7BA",
	background = "#12120F",
	tab_bar = {
		active_tab = {
			bg_color = "#394e7d",
			fg_color = "#fafafa",
			intensity = "Bold",
		},
	},
}

config.tab_bar_at_bottom = true
config.window_frame = {
	font = wezterm.font({ family = "Geist", weight = "Bold" }),
	font_size = 10.0,
	active_titlebar_bg = "rgba(0 0 0 0)",
}

config.initial_rows = 40
config.initial_cols = 120

config.window_padding = {
	left = 7,
	right = 7,
	top = 7,
	bottom = 0,
}

--config.default_prog = { "bash", "-c", 'zellij -s "$(random5chr)"' }

return config
